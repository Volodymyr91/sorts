package root;

public enum Sorter {
    BUBBLE {
        @Override
        public void sort(int[] array) {
            int i, j;
            for (i = 0; i < array.length - 1; i++) {
                for (j = 0; j < array.length - 1 - i; j++) {
                    if (getCounter().greater(array[j], array[j + 1])) {
                        getCounter().swap(array, j, j + 1);
                    }
                }
            }
        }
    }, HEAP {
        @Override
        public void sort(int[] array) {
            int n = array.length - 1;
            buildHeap(array);
            for (int i = n; i > 0; i--) {
                getCounter().swap(array, 0, i);
                n--;
                maxheap(array, 0, n);
            }
        }
    }, QUICK {
        @Override
        public void sort(int[] array) {
            doQuickSort(0, array.length - 1, array);
        }
    }, SHELL {
        @Override
        public void sort(int[] array) {
            int n = array.length;
            int h = n / 2;

            while (h >= 1) {
                for (int i = h; i < n; i++) {
                    for (int j = i; j >= h && getCounter().greater(array[j - h], array[j]); j -= h) {
                        getCounter().swap(array, j, j - h);
                    }
                }
                h = h / 2;
            }
        }
    },
    STRAIGHT_INSERTION {
        @Override
        public void sort(int[] array) {
            for (int i = 0; i < array.length; i++) {
                for (int j = i; j > 0; j--) {
                    if (getCounter().greater(array[j - 1], array[j]))
                        getCounter().swap(array, j, j - 1);
                    else break;
                }
            }

        }
    };

    public abstract void sort(int[] array);

    private static void doQuickSort(int left, int right, int[] array) {
        int i = left;
        int j = right;
        int pivot = array[(left + right) / 2];

        do {
            while (getCounter().greater(pivot, array[i])) i++;
            while (getCounter().greater(array[j], pivot)) j--;

            if (i <= j) {
                getCounter().swap(array, i, j);
                i++;
                j--;
            }
        } while (i <= j);

        if (left < j) {
            doQuickSort(left, j, array);
        }

        if (i < right) {
            doQuickSort(i, right, array);
        }
    }

    private static void buildHeap(int[] array) {
        int n = array.length - 1;
        for (int i = n / 2; i >= 0; i--) {
            maxheap(array, i, n);
        }
    }

    private static void maxheap(int[] array, int i, int n) {
        int left = 2 * i;
        int right = 2 * i + 1;
        int largest;
        if (left <= n && getCounter().greater(array[left], array[i])) {
            largest = left;
        } else {
            largest = i;
        }

        if (right <= n && getCounter().greater(array[right], array[largest])) {
            largest = right;
        }
        if (largest != i) {
            getCounter().swap(array, i, largest);
            maxheap(array, largest, n);
        }
    }


    private static PerformanceCounter getCounter() {
        return PerformanceCounter.getInstance();
    }

    protected static class PerformanceCounter {
        private long swapCount = 0;
        private long comparisonCount = 0;
        private static PerformanceCounter instance;

        private PerformanceCounter() {
        }

        public void swap(int[] array, int i, int j) {
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            swapCount++;
        }

        public boolean greater(int i, int j) {
            comparisonCount++;
            return i > j;
        }

        public long getSwapCount() {
            return swapCount;
        }

        public long getComparisonCount() {
            return comparisonCount;
        }

        public void resetAllCounters() {
            swapCount = 0;
            comparisonCount = 0;
        }

        public static PerformanceCounter getInstance() {
            if (instance == null) {
                instance = new PerformanceCounter();
            }
            return instance;
        }
    }
}
