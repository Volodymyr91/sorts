package root;

import java.util.Random;

import static root.Properties.getInstance;

public enum ArrayGenerator {
    BACK_SORTED {
        @Override
        public int[] generate() {
            int[] result = new int[getInstance().getArrraySize()];
            for (int i = 0; i < result.length; i++) {
                result[i] = result.length - i;
            }
            return result;
        }
    }, CONSTANT_SORTED {
        private static final int CONSTANT_NUMBER = 1;

        @Override
        public int[] generate() {
            int[] result = new int[getInstance().getArrraySize()];
            for (int i = 0; i < result.length; i++) {
                result[i] = CONSTANT_NUMBER;
            }
            return result;
        }
    }, PARTLY_SORTED {
        @Override
        public int[] generate() {
            int[] result = new int[getInstance().getArrraySize()];
            int blockSize = getInstance().getBlockSize();
            int n = 1;
            for (int i = 0; i < result.length; i++) {
                result[i] = n++;
                if (n == blockSize + 1) {
                    n = 1;
                }
            }

            return result;
        }
    }, RANDOM {
        @Override
        public int[] generate() {
            int[] result = new int[getInstance().getArrraySize()];
            Random rand = new Random();
            for (int i = 0; i < result.length; i++) {
                result[i] = rand.nextInt(result.length);
            }
            return result;
        }
    }, SORTED {
        @Override
        public int[] generate() {
            int[] result = new int[getInstance().getArrraySize()];
            for (int i = 0; i < result.length; i++) {
                result[i] = i;
            }
            return result;
        }
    };

    public abstract int[] generate();
}
