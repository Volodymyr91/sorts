package root;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Application {
    private static final String TABLE_HEADING = "Array_length swap cmp swap cmp swap cmp swap cmp swap cmp",
            FILE_NAME = "exel.xls";

    public static void main(String[] args) {
        Sorter.PerformanceCounter counter = Sorter.PerformanceCounter.getInstance();
        try {
            PrintWriter writer = new PrintWriter(FILE_NAME);
            for (Sorter sorter : Sorter.values()) {
                writer.write(sorter.name() + " ");
                writeArrayGeneratorNames(writer);
                writer.write("\n" + TABLE_HEADING + "\n"
                        + Properties.getInstance().getArrraySize() + " ");
                writeSortResults(writer);
                writer.write("\n\n");
            }
            writer.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeArrayGeneratorNames(PrintWriter writer) {
        for (ArrayGenerator generator : ArrayGenerator.values()) {
            writer.write(generator.name() + "  ");
        }
    }

    private static void writeSortResults(PrintWriter writer) {
        for (ArrayGenerator generator : ArrayGenerator.values()) {
            sorter.sort(generator.generate());
            writer.write(counter.getSwapCount() + " "
                    + counter.getComparisonCount() + " ");
            counter.resetAllCounters();
        }
    }
}

