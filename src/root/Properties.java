package root;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Properties extends java.util.Properties {
    private static Properties instance;

    private Properties() {
    }

    public static Properties getInstance() {
        if (instance == null) {
            instance = new Properties();
        }
        instance.load();
        return instance;
    }

    public int getArrraySize() {
        return Integer.parseInt(instance.getProperty("arraysize"));
    }

    public int getBlockSize() {
        return Integer.parseInt(instance.getProperty("partlysortedarrayblocksize"));
    }

    private Properties load() {
        try {
            InputStream inputStream = new FileInputStream("config.properties");
            instance.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return instance;
    }

}
